<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'lime-digital' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '&e!K~ft3R&n=GAw&TE0YuMk-,:64b<|5A+M,MuI0<9Pp+a)xb53;<V^F/1DYm/@&' );
define( 'SECURE_AUTH_KEY',  '0T/`Ggz(=ScZ`n@!f/~]?F=?N/||nyHpN;4@0y2b{?kEdG13wukhmjfs}y8l~y+1' );
define( 'LOGGED_IN_KEY',    '/@iI{Y9)Ygy e)NsrSsUDB~T[>o*MD{yX>[EZaHeOh8{g.L}0ykzQie$d<!@?$uL' );
define( 'NONCE_KEY',        'eE;v7tj=<85 I*fYJ`_dOXF.L^IR6o[sr(1YI63H@w)=K#GL0!~=6<g5PH^^y/!<' );
define( 'AUTH_SALT',        'LGTEtPNw vUn6C~*f/}SU5&$:lpZ7jK$q0F_+2Ji_-?O|<<q_7HqanQF+ b+KWS]' );
define( 'SECURE_AUTH_SALT', '%Z^};DF2B)&@.uSaLY}<ZQqiF5Qp#>}T+!@d|KwsY=>ciT1cA2z51o+Q-~ACj2,|' );
define( 'LOGGED_IN_SALT',   '2Coh)Ew3`|uq}C/R //*GrHLr@jb.,6G.w(d^mk!<CuXerw,U)b55rPVez#.B#j=' );
define( 'NONCE_SALT',       'vop6D2Ag#$[_yz-V_>+a7S;9{RYE$2[(2!3LwkX1];y{;]! |~&:ZC~Xnd#Ny`hA' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ld_';
define('WP_MEMORY_LIMIT', '256M');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
